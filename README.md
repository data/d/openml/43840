# OpenML dataset: Dow-Jones-Industrial-Average-Dataset

https://www.openml.org/d/43840

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Context
The Dow Jones Industrial Average is one of the most followed stock market indexes by investors, financial professionals and the media. 
Content
It measures the daily price movements of 30 large American companies on the Nasdaq and the New York Stock Exchange. The Dow Jones Industrial Average is widely viewed as a proxy for general market conditions and even the economy of the United States.
This Dow Jones Industrial Average dataset is downloaded from https://www.investing.com/indices/us-30-historical-data, including 2767 closing records from January 4th 2009 to December 31st 2019.

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43840) of an [OpenML dataset](https://www.openml.org/d/43840). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43840/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43840/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43840/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

